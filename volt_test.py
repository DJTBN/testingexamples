import unittest
from volt import Volt


class VoltDriveTest(unittest.TestCase):

    def test_car_starts_with_battery(self):
        car = Volt()
        self.assertTrue(car.get_battery_status() > 0)

    def test_battery_goes_down_on_drive(self):
        car = Volt()
        original_battery = car.get_battery_status()
        car.drive()
        new_battery = car.get_battery_status()
        self.assertNotEqual(original_battery, new_battery)


if __name__ == '__main__':
    unittest.main()
