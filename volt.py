import random


class Volt():
    battery = random.randint(0, 100)
    generator_status = "off"
    battery_threshold = 50
    fuel_level = 50
    low_fuel_threshold = 10
    fuel_economy = 25
    battery_economy = 50
    low_fuel_warning = False

    """Returns the current battery status of the Volt

    Returns:
        int -- Integer percentage representing battery status
    """

    def get_battery_status(self):
        if self.battery < 50:
            self.generator_status = "on"
        else:
            self.generator_status = "off"
        return self.battery

    """Returns current generator status. "on" for on and "off" for off

    Returns:
        string -- current generator status
    """

    def get_generator_status(self):
        return self.generator_status

    """Simulates driving the Volt for a specified number of miles. If no miles are specified,
    a random number between 1 and 1250 is chosen

    Returns:
        None
    """

    def drive(self, miles=None):
        if not miles:
            miles = random.randint(1, 1250)
        battery_available = self.battery - self.battery_threshold
        fuel_used = miles // self.fuel_economy
        self.fuel_level -= fuel_used

    """Returns current stats of the Volt

    Returns:
        Dict -- current volt stats
    """

    def get_stats(self):
        return {
            "Low Fuel Warning": self.low_fuel_warning,
            "Battery": self.battery,
            "Fuel Level": self.fuel_level,
            "Generator Status": self.generator_status}
